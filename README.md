# README #

This repository creates in AWS:

* one VPC
* 4 subnets
* 4 security groups
* 3 Elastic IPs (for management, production, development)
* 1 routing table
* 1 internet gateway
* 6 EC2 ubuntu 16.04 instances

###Basic setup###
This is a project that spins off 6 EC2 instances in order to form an architecture of one development machine, one management machine, one proxy server, two webservers and one database server.
The project requires the boto python module so that we are able to communicate with AWS. The easy way to install it is :

```
#!bash

pip install boto3
```
 
Having Ansible already installed on the local laptop, we first have to encrypt the cred_vars.yml file. To do that we use ansible-vault encrypt vars/cred_vars.yml and we put our desired password. The file itself contains the access key, the secret key and the name of the private key, all generated through AWS.



### How do I get set up? ###
* Install Ansible locally
* Install pip
* Install boto
* clone the repo
* create your own vars/cred_vars.yml file in the form of

```
#!yaml
---
myaccesskey: "AKIXXXXXXXXXXXXXXXXXX"
mysecretkey: "XXXXXXXXXXXXXXXXXXXXXXXXXX"
mykeypair: XXXXXXXXX

```
* encrypt the file with ansible-vault encrypt vars/cred_vars.yml
* run sudo ansible-playbook -i hosts site.yml --ask-vault-pass
* fill in the required password
* after the whole play finishes you have to copy the secret key into the management node, so that the other nodes are accessible through that one. Easy way to do that is the 
scp -i <my_secret.pem> <my_secret.pem> ssh ubuntu@<first-ip-you-get-as-output-from-the-play>:~/.ssh

Finally by log on to the management node with

ssh -i <my_secret.pem> ubuntu@<first-ip-you-get-as-output-from-the-play> 

and follow the steps on my next repo.